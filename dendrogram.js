var margin = {top: 20, right: 120, bottom: 20, left:120};
var width = 960 - margin.right - margin.left;
var hieght = 800 - margin.top - margin.bottom;

var i = 0,
var duration = 750;
var root;

var tree = d3.cluster.size([360, 390]).separation(function(a, b) { return (a.parent == b.parent ? 1 : 2) / a.depth; });

var diagonal = d3.svg.diagonal().projection(function(d) { return [d.y, d.x]; });

var svg = d3.select("svg").append("svg")
    .attr("width", width + margin.right + margin.left)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

d3.csv("cleaned.csv", function(error, data){
    if (error) throw error;

    var root = tree(stratify(data).sort(function(a,b){
        return (a.height - b.height) || a.id.localeCompare(b.id);
    }));

    var link = g.selectAll(".link").data(root.descendants().slice(1)).enter()
        .enter().append("path").attr("class", "link").attr("d", function(d){
            return "M" + project(d.x, d.y)
            + "C" + project(d.x, (d.y + d.parent.y) / 2)
            + " " + project(d.parent.x, (d.y + d.parent.y) / 2)
            + " " + project(d.parent.x, d.parent.y);
        });

    var node = g.selectAll(".node")
        .data(root.descendants())
        .enter().append("g")
          .attr("class", function(d) { return "node" + (d.children ? " node--internal" : " node--leaf"); })
          .attr("transform", function(d) { return "translate(" + project(d.x, d.y) + ")"; });
    
    node.append("circle").attr("r", 2.5);
    
    node.append("text")
    .attr("dy", ".31em")
    .attr("x", function(d) { return d.x < 180 === !d.children ? 6 : -6; })
    .style("text-anchor", function(d) { return d.x < 180 === !d.children ? "start" : "end"; })
    .attr("transform", function(d) { return "rotate(" + (d.x < 180 ? d.x - 90 : d.x + 90) + ")"; })
    .text(function(d) { return d.id.substring(d.id.lastIndexOf(",") + 1); });
});

function project(x, y) {
    var angle = (x - 90) / 180 * Math.PI, radius = y;
    return [radius * Math.cos(angle), radius * Math.sin(angle)];
}
