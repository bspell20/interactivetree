# InteractiveTree
Creates an interactive dendrogram using D3 from the first three columns of the data in the order 'Profession', 'Company' and 'Name'. 

The data is from the Entertainment.csv file.

The cleaned data will is in the output.csv file.
