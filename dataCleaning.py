
import csv

fileName = "Entertainment.csv"

with open(fileName) as file:
    with open("cleaned.csv","w") as fileOut:
        writer= csv.writer( fileOut )
        for row in csv.reader(file):
            writer.writerow( (row[0], row[1], row[2]) )
        
